
// 取得header
window.onload = function () {
    
}


// 按下登入按鈕
function Login() {
    // 取得畫面上輸入的帳號、密碼
    var account = document.getElementsByClassName("account")[0].value;
    var password = document.getElementsByClassName("password")[0].value;
    
    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/Login/Login";

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json")
    
    // 組成object並轉為Json格式
    data = { Account : account, Password : password };
    console.log(JSON.stringify(data));

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            response = JSON.parse(xhr.response);
            
            // 取得token
            token = response[0].Value;
            // 把token放到cookie
            document.cookie = 'token=' + token;

            // 確認個人資料是否填寫完整，否則導向個人資料設定
            if (response[1].Value[0] == 'False'){
                window.location.replace("http://127.0.0.1:5500/MemberSetting.html");
            }
            else {
                location.reload();
            }
        };
    };
    xhr.send(JSON.stringify(data));
}

// 確定是否有登入 (取token)
window.onload = function () {
    var token = document.cookie.length;
    if (token>0) {
        document.getElementsByClassName("login")[0].style.display="none";
    }
}
