
// 取得公司名稱下拉
var xhr1 = new XMLHttpRequest();
var url1 = "https://localhost:44308/api/Company/GetCompanyName";

xhr1.open("GET", url1, true);
xhr1.setRequestHeader("Content-Type", "application/json")

xhr1.onload = function () {
    if(xhr1.status >= 200 && xhr1.status < 400) {
        response = JSON.parse(xhr1.response);
        console.log(response);
        GetCompanySelect(response);
    };
};
xhr1.send();

function GetCompanySelect (Data) {
    for (var i in Data){
        var Id = Data[i].CompanyId;
        var Name = Data[i].CompanyName;
    
        var parentDiv = document.getElementsByClassName("select-company")[0];
        var insertDiv = '<option value='+Id+'>'+Name+'</option>';
        parentDiv.insertAdjacentHTML('beforeend', insertDiv);
    }
}

// 取得成員名字下拉
var xhr2 = new XMLHttpRequest();
var url2 = "https://localhost:44308/api/MemberManagement/GetAllMemberName";

xhr2.open("GET", url2, true);
xhr2.setRequestHeader("Content-Type", "application/json")

xhr2.onload = function () {
    if(xhr2.status >= 200 && xhr2.status < 400) {
        response = JSON.parse(xhr2.response);
        console.log(response);
        GetMemberSelect(response.MemberDataList);
    };
};
xhr2.send();

function GetMemberSelect (Data) {
    for (var i in Data){
        var Id = Data[i].MemberDatasId;
        var Name = Data[i].Name;
    
        var parentDiv = document.getElementsByClassName("select-member")[0];
        var insertDiv = '<option value='+Id+'>'+Name+'</option>';
        parentDiv.insertAdjacentHTML('beforeend', insertDiv);
    }
}


// 新增資料到DB
function insertData () {
    // 身份
    var Role;
    // 公司名稱
    var Company = document.getElementsByClassName("select-company")[0].value;
    // 成員
    var Member = document.getElementsByClassName("select-member")[0].value;
    // 職稱
    var Position =  document.getElementsByClassName("position-input")[0].value;
    // 入職日期
    var EntryDate =  document.getElementsByClassName("entryDate-input")[0].value;
    // 離職日期 (可null)
    var LeaveDate =  document.getElementsByClassName("leavedate-input")[0].value;
    // 工作內容
    var Duty =  document.getElementsByClassName("duty-input")[0].value;
    // 心得 (可null)
    // var Feedback =  document.getElementsByClassName("feedback-input")[0].value;

    // 檢查欄位是否有填寫
    var isfillUp = false;
    if (Company == "") {
        alert("請填寫公司名稱");
    }
    else if (Member == "") {
        alert("請填寫姓名");
    }
    else if (Position == "") {
        alert("請填寫職稱");
    }
    else if (EntryDate == "") {
        alert("請填寫入職日期");
    }
    else if (Duty == "") {
        if (Role == 3) {
            alert("請填寫工作內容");
        }
        else {
            document.getElementsByClassName("duty-input")[0].value="（待修改）";
            isfillUp = true;
        }
    }
    else {
        isfillUp = true;
    }
    
    if (isfillUp) {
        console.log(1);
        // 新增到資料庫
        var xhr = new XMLHttpRequest();
        var url = "https://localhost:44308/api/WorkExperience/CreateWorkData";

        // 組成object並轉為Json格式
        data = { 
            CompanyType : Company,
            MemberId : Member,
            Position : Position,
            EntryDate : EntryDate,
            LeaveDate : LeaveDate,
            WorkDuty : Duty
        };
        console.log(JSON.stringify(data));

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        var token = document.cookie.split("=")[1];
        xhr.setRequestHeader("Authorization", "Bearer " + token);

        xhr.onload = function () {
            if(xhr.status >= 200 && xhr.status < 400) {
                window.location.replace("http://127.0.0.1:5500/Works.html");
            };
        };
        xhr.send(JSON.stringify(data));
    }
}

