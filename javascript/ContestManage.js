// 重整頁面帶值以切換側欄
window.onload=function (){
    companyCount = document.getElementsByClassName("contest-edit").length;
    count = companyCount;
}

// 技能管理 檢查輸入框是否有值
function checkInput(id){
    var checkId = 'contest' + id;
    var inputbox = document.getElementById(checkId).value;
    var parent = document.getElementsByClassName("contests")[0].lastElementChild;
    var check = document.getElementsByClassName(checkId)[0];
    //確認這筆是最後一筆
    if (parent == check){
        //確認這筆不為空
        if (inputbox!=""){
            newDelBtn(id);
            newContest();
        }
    }
    
}

// 新增刪除按鈕
function newDelBtn(id){
    delId = 'del'+id;
    var delBlock = document.getElementById(delId);
    var delbtn = '<img src="icon/delete.png" onclick="delContest('+id+')">';
    delBlock.insertAdjacentHTML('beforeend', delbtn);
}

// 技能管理的欄位新增
function newContest(){
    count += 1;
    var parentDiv = document.getElementsByClassName("contests")[0];
    var insertDiv = '<div class="contest'+count+' contest">'+
                    '<input class="contest-edit" id="contest'+count+'" type="text" placeholder="競賽名稱" oninput="checkInput('+count+')">'+
                    '<input class="contest-date" id="date'+count+'" type="date">'+
                    '<div class="del-btn" id="del'+count+'"></div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('beforeend', insertDiv);
}

// 刪除一筆技能欄位
function delContest(id){
    var delId = '.contest'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}