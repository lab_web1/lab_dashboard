// 重整頁面帶值以切換側欄
window.onload=function (){
    skillCount = document.getElementsByClassName("skill-edit").length;
    count = skillCount;
}

// 技能管理 檢查輸入框是否有值
function checkInput(id){
    var checkId = 'skill' + id;
    var inputbox = document.getElementById(checkId).value;
    var parent = document.getElementsByClassName("skills")[0].lastElementChild;
    var check = document.getElementsByClassName(checkId)[0];
    //確認這筆是最後一筆
    if (parent == check){
        //確認這筆不為空
        if (inputbox!=""){
            newDelBtn(id);
            newSkill();
        }
    }
    
}

// 新增刪除按鈕
function newDelBtn(id){
    delId = 'del'+id;
    var delBlock = document.getElementById(delId);
    var delbtn = '<img src="icon/delete.png" onclick="delSkill('+id+')">';
    delBlock.insertAdjacentHTML('beforeend', delbtn);
}

// 技能管理的欄位新增
function newSkill(){
    count += 1;
    var parentDiv = document.getElementsByClassName("skills")[0];
    var insertDiv = '<div class="skill'+count+' skill">'+
                    '<input class="skill-edit" id="skill'+count+'" type="text" oninput="checkInput('+count+')" placeholder="請輸入技能名稱">'+
                    '<div class="del-btn" id="del'+count+'"></div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('beforeend', insertDiv);
}

// 修改技能儲存
function update(id) {
    alert('(更新資料庫中的技能)');
}

// 確認此技能是否可以刪除
function delCheck (id) {
    alert('(確認members的技能)');
    alert('(刪除資料庫中的技能)');
    delSkill(id);
}

// 刪除一筆技能欄位
function delSkill(id){
    var delId = '.skill'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}