window.onload = function () {
    var parentList = document.getElementsByClassName("list")[0];
    var parentRight = document.getElementsByClassName("header-r")[0];

    var List = '<li class="a-topic topic" onclick="location.href=\'MembersManage.html\'">成員管理</li>'+
                        '<li class="a-topic topic select">榮譽榜'+
                            '<ul class="dropdown list">'+
                                '<li class="b-topic topic" onclick="location.href=\'Contest.html\'">競賽紀錄</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'Subject.html\'">專題紀錄</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'Project.html\'">研究計畫</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'Essay.html\'">碩士論文</li>'+
                            '</ul>'+
                        '</li>'+
                        '<li class="a-topic topic" onclick="location.href=\'Works.html\'">經歷</li>'+
                        '<li class="a-topic topic" onclick="location.href=\'SkillsManage.html\'">技能管理</li>'+
                        '<li class="a-topic topic select">名稱管理'+
                            '<ul class="dropdown list">'+
                                '<li class="b-topic topic" onclick="location.href=\'ContestManage.html\'">競賽名稱</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'SubjectManage.html\'">專題名稱</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'CompanyManage.html\'">公司名稱</li>'+
                            '</ul>'+
                        '</li>'+
                    '</ul>';

    var RightBlock = '<ul>'+
                        '<li class="a-topic topic select">'+
                            '<div class="member-info">'+
                                '<div class="member-image">'+
                                    '<img src="icon/male.jpg">'+
                                '</div>'+
                            '</div>'+
                            '<ul class="dropdown list">'+
                                '<li class="b-topic topic" onclick="location.href=\'MemberSetting.html\">資料設定</li>'+
                                '<li class="b-topic topic" onclick="location.href=\'MemberSetting.html\">榮譽/經歷</li>'+
                                '<li class="b-topic topic"><button class="register-btn">登出</button></li>'+
                            '</ul>'+
                        '</li>'+
                    '</ul>';

    parentList.insertAdjacentHTML('beforeend', List);
    parentRight.insertAdjacentHTML('afterbegin', RightBlock);
}
