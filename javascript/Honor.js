/*


// 側欄切換
function pagechange(type){
    var id="";
    // 標題
    var a = document.getElementsByClassName("topic-2")[0];
    // 新增紀錄的四顆按鈕先設為隱藏
    document.getElementById("btn-c").style.display="none";
    document.getElementById("btn-s").style.display="none";
    document.getElementById("btn-p").style.display="none";
    document.getElementById("btn-e").style.display="none";
    // 搜尋欄
    var nameSelect = document.getElementsByClassName('search')[0].firstElementChild;
    var searchDiv = document.getElementsByClassName('search2')[0];
    var searchBox = document.getElementsByClassName('search2')[0].firstElementChild;

    if(type=="contest"){
        id="#radio-c";
        a.innerHTML = "競賽紀錄";
        var b= document.getElementById("btn-c");
        b.style.display="flex";
        // 搜尋欄更動
        nameSelect.style.display="flex";
        searchDiv.style.width="40vw";
        searchBox.style.width="40vw";
        searchBox.placeholder="請輸入競賽組別、作品名稱、參賽人員";
    }
    else if(type=="subject"){
        id="#radio-s";
        a.innerHTML = "專題紀錄";
        var b= document.getElementById("btn-s");
        b.style.display="flex";
        nameSelect.style.display="flex";
        // 搜尋欄更動
        searchDiv.style.width="40vw";
        searchBox.style.width="40vw";
        searchBox.placeholder="請輸入專題組別、作品名稱、專題成員";
    }
    else if(type=="project"){
        id="#radio-p";
        a.innerHTML = "研究計劃";
        var b= document.getElementById("btn-p");
        b.style.display="flex";
        nameSelect.style.display="none";
        // 搜尋欄更動
        searchDiv.style.width="55vw";
        searchBox.style.width="55vw";
        searchBox.placeholder="請輸入研究計畫、計畫類型、研究計畫名稱、研究人員";
    }
    else if(type=="essay"){
        id="#radio-e";
        a.innerHTML = "碩士論文";
        var b= document.getElementById("btn-e");
        b.style.display="flex";
        nameSelect.style.display="none";
        // 搜尋欄更動
        searchDiv.style.width="55vw";
        searchBox.style.width="55vw";
        searchBox.placeholder="請輸入論文名稱、論文類型、研究人員";
    }
    document.querySelector(id).checked = true;
}
window.onload=function (){
    var url=location.href;
    //額外成員計數
    extraMemberCount = document.getElementsByClassName("chk-extraMembers").length;
    countM = extraMemberCount;
    //檔案計數
    filesCount = document.getElementsByClassName("chk-files").length;
    countF = filesCount;
    
    if(url.indexOf('?')!=-1){
       var str=url.split('?');
       var str2=str[1] 
    }else{
        var str2='contest';
    }
    pagechange(str2);
    
}

*/


window.onload=function (){
    //額外成員計數
    extraMemberCount = document.getElementsByClassName("chk-extraMembers").length;
    countM = extraMemberCount;
    //檔案計數
    filesCount = document.getElementsByClassName("chk-files").length;
    countF = filesCount;
    
}

// [新增、修改] 依成員人數取得空白欄位
function countpeople(){
    var obj = document.getElementsByName("mem").length;
    if(obj!=0){
        for (var i=obj-1; i>=0; i--){
            console.log('del'+i);
            document.getElementsByName('mem')[i].outerHTML="";
        }
        //document.getElementsByName('mem').outerHTML="";
    }

    // 取得人數，總共要新增的input數量
    var num = document.getElementById("member").value;
    
    // 要新增的input內容
    var html2 = '<tr name="mem">'+
                '<td class="m-title">組員</td>'+
                '<td class="m-content">'+
                '<select class="inputbox3" id="member">'+
                '<option>請選擇</option>'+
                '<option>1</option>'+
                '<option>2</option>'+
                '<option>3</option>'+
                '<option>4</option>'+
                '<option>5</option>'+
                '<option>6</option>'+
                '<option>7</option>'+
                '<option>8</option>'+
                '<option>9</option>'+
                '<option>10</option>'+
                '</select>'+
                '</td>'+
                '</tr>';

    // document.querySelector('#members').insertAdjacentHTML('afterbegin','<tbody id="del"></tbody>');
    for(i=1;i<=num;i++){
        //要在什麼位置新增input
        var parent = document.getElementById('members').lastElementChild;
        // console.log("insert"+i);
        parent.insertAdjacentHTML('afterend', html2);
    }  
}



// [新增、修改] 額外成員checkbox判斷
function extraMember(id) {
    var checkId = 'chk-exm'+id;
    var isChecked = document.getElementById(checkId).checked;
    if (isChecked) {
        newMember();
    }
    else {
        delMember(id);
    }
    // alert(isChecked);
}

// 額外成員的欄位新增
function newMember(){
    countM += 1;
    var parentTbody = document.getElementsByClassName("chk-extraMembers")[0];
    var insertTr = '<tr class="chk'+countM+' chk">'+
                   '<td class="m-title"><input type="checkbox"  class="check" id="chk-exm'+countM+'" onchange="extraMember('+countM+')"><label for="chk-exm'+countM+'">額外成員</label></td>'+
                   '<td class="m-content"><input class="inputbox3" type="text"></td>'+
                   '</tr>';
    parentTbody.insertAdjacentHTML('beforeend', insertTr);
}

// 刪除額外成員欄位
function delMember(id){
    var delId ='.chk'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}



// [新增、修改] 檔案checkbox判斷
function fileCheck(id) {
    var checkId = 'chk-file'+id;
    var isChecked = document.getElementById(checkId).checked;

    if (isChecked) {
        newFile();
    }
    else {
        var yes = confirm('你確定要刪除嗎？')
        if (yes){
            delFile(id);
        }
        else{
            document.getElementById(checkId).checked="true";
        }
    }
}

// 檔案的欄位新增
function newFile(){
    countF += 1;
    var parentTbody = document.getElementsByClassName("chk-files")[0];
    var insertTr = '<tr class="chk-file'+countF+' chk-file">'+
                    '<td>'+
                    '<input type="checkbox"  class="check" id="chk-file'+countF+'" onchange="fileCheck('+countF+')">'+
                    '<input class="upload" type="file">'+
                    '</td>'+
                    '</tr>';
    parentTbody.insertAdjacentHTML('beforeend', insertTr);
}

// 刪除檔案欄位
function delFile(id){
    var delId ='.chk-file'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}