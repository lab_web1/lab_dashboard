// 重整頁面帶值以切換側欄
window.onload=function (){
    subjectCount = document.getElementsByClassName("subject-edit").length;
    count = subjectCount;
}

// 技能管理 檢查輸入框是否有值
function checkInput(id){
    var checkId = 'subject' + id;
    var inputbox = document.getElementById(checkId).value;
    var parent = document.getElementsByClassName("subjects")[0].lastElementChild;
    var check = document.getElementsByClassName(checkId)[0];
    //確認這筆是最後一筆
    if (parent == check){
        //確認這筆不為空
        if (inputbox!=""){
            newDelBtn(id);
            newSubject();
        }
    }
    
}

// 新增刪除按鈕
function newDelBtn(id){
    delId = 'del'+id;
    var delBlock = document.getElementById(delId);
    var delbtn = '<img src="icon/delete.png" onclick="delSubject('+id+')">';
    delBlock.insertAdjacentHTML('beforeend', delbtn);
}

// 技能管理的欄位新增
function newSubject(){
    count += 1;
    var parentDiv = document.getElementsByClassName("subjects")[0];
    var insertDiv = '<div class="subject'+count+' subject">'+
                    '<input class="subject-edit" id="subject'+count+'" type="text" placeholder="專題學年度" oninput="checkInput('+count+')">'+
                    '<input class="subject-date" id="date'+count+'" type="date">'+
                    '<div class="del-btn" id="del'+count+'"></div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('beforeend', insertDiv);
}

// 刪除一筆技能欄位
function delSubject(id){
    var delId = '.subject'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}