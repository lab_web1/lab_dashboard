
var xhr = new XMLHttpRequest();
var url = "https://localhost:44308/api/MemberManagement/MemberDatas";

xhr.open("GET", url, true);
xhr.setRequestHeader("Content-Type", "application/json")

xhr.onload = function () {
    if(xhr.status >= 200 && xhr.status < 400) {
        response = JSON.parse(xhr.response);
        console.log(response.MemberDataList);
        GetData(response.MemberDataList);
    };
};
xhr.send();

function GetData (Data) {
    for (var i in Data) {
        // ID
        var Id = Data[i].MemberDatasId;
        // 照片
        var ImageUrl = Data[i].ImageUrl;
        // 姓名
        var Name = Data[i].Name;
        // 身份
        var RoleId = Data[i].Member.RoleId;
        var Role = Data[i].Member.Role.RoleName + " ";
        var Session = Data[i].Member.Session;
        var isGraduation = Data[i].Member.isGraduation;
        if (RoleId == 2) {
            Role += Session + "年";
        }
        else if (Role == 3) {
            Role += Session + ".0";
        }
        if (isGraduation) {
            Role += "（畢業）";
        }
        // 帳號
        var Account = Data[i].Member.Account;
        // 電話
        var Phone = Data[i].Phone=="" ? "尚未填寫" : Data[i].Phone;
        // 信箱
        var Email = Data[i].Email=="" ? "尚未填寫" : Data[i].Email;

        // 塞資料
        var parentDiv = document.getElementsByClassName("members")[0];
        var insertDiv = '<div class="member member'+Id+'" onclick="location.href=\'MemberEdit.html?Id='+Id+'\'">\
                            <div class="personal">\
                                <div class="person-image">\
                                    <img src="icon/male.jpg">\
                                </div>\
                                <div class="person-info">\
                                    <table>\
                                        <tr>\
                                            <td class="t-name">'+Name+' / <span>'+Role+'</span></td>\
                                        </tr>\
                                        <tr>\
                                            <td class="t-content"><img src="icon/account.png">'+Account+'</td>\
                                        </tr>\
                                        <tr>\
                                            <td class="t-content"><img src="icon/phone.png">'+Phone+'</td>\
                                        </tr>\
                                        <tr>\
                                            <td class="t-content"><img src="icon/email.png">'+Email+'</td>\
                                        </tr>\
                                    </table>\
                                </div>\
                            </div>\
                        </div>';
        parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
    }
    newInsertbtn();
}


// 重整頁面時加入新增按鈕
function newInsertbtn(){
    var parentDiv = document.getElementsByClassName("members")[0];
    var insertDiv = '<div class="member create" onclick="location.href=\'MemberInsert.html\'">'+
                    '<img src="icon/add.png">'+
                    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
}


// 側欄checkbox
function chkChange (str) {
    // 全部
    var chkAll = document.getElementById("chk-all").checked;
    // 已畢業
    var chkGra = document.getElementById("chk-graduated").checked;
    // 研究生
    var chkpostGra = document.getElementById("chk-postgraduate").checked;
    var selpostGra = document.getElementsByClassName("sel-postgraduate")[0];
    // 專題生
    var chkunderGra = document.getElementById("chk-undergraduate").checked;
    var selunderGra = document.getElementsByClassName("sel-undergraduate")[0];

    if (str == 'all') {
        if (chkAll == true) {
            document.getElementById("chk-graduated").checked = false;
            document.getElementById("chk-postgraduate").checked = false;
            document.getElementById("chk-undergraduate").checked = false;
        }
        else {

        }
    }
    else if (str == 'gra') {
        if (chkGra == true){
            document.getElementById("chk-all").checked = false;
        }
    }
    else if (str == 'postgra') {
        if (chkpostGra == true){
            document.getElementById("chk-all").checked = false;
        }
    }
    else if (str == 'undergra') {
        if (chkunderGra == true){
            document.getElementById("chk-all").checked = false;
        }
    }
}

