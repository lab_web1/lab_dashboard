
// 取資料 (資料的的資料)
var xhr = new XMLHttpRequest();
var url = "https://localhost:44308/api/Company/GetCompanyName";

xhr.open("GET", url, true);
xhr.setRequestHeader("Content-Type", "application/json")

xhr.onload = function () {
    if(xhr.status >= 200 && xhr.status < 400) {
        response = JSON.parse(xhr.response);
        GetData(response);
    };
};
xhr.send();

// 塞資料 (資料的的資料)
function GetData(Data) {
    count = Data.length;
    for (var i in Data) {
        OldCompanyId = Data[i].CompanyId;
        OldCompanyName = Data[i].CompanyName;
        var parentDiv = document.getElementsByClassName("companies")[0];
        var insertDiv = '<div class="oldCompany'+OldCompanyId+' company">\
                        <input class="company-edit" id="oldCompany'+OldCompanyId+'" type="text" onfocusout="updateComapny('+OldCompanyId+')" placeholder="請輸入公司名稱" value="'+OldCompanyName+'">\
                        <div class="del-btn" id="del'+OldCompanyId+'" onclick="delCompany('+OldCompanyId+')">\
                        <img src="icon/delete.png"></div>\
                        </div>';
        parentDiv.insertAdjacentHTML('beforeend', insertDiv);
    }
    newCompany();
}

// 檢查輸入框是否有值
function checkInput(id){
    var checkId = 'company' + id;
    var inputbox = document.getElementById(checkId).value;
    var parent = document.getElementsByClassName("companies")[0].lastElementChild;
    var check = document.getElementsByClassName(checkId)[0];
    //確認這筆是最後一筆
    if (parent == check){
        //確認這筆不為空
        if (inputbox!=""){
            newDelBtn(id);
            newCompany();
        }
    }
    
}

// 新增刪除按鈕
function newDelBtn(id){
    delId = 'del'+id;
    var delBlock = document.getElementById(delId);
    var delbtn = '<img src="icon/delete.png"">';
    delBlock.insertAdjacentHTML('beforeend', delbtn);
}

// 公司管理的欄位新增
function newCompany(){
    count += 1;
    var parentDiv = document.getElementsByClassName("companies")[0];
    var insertDiv = '<div class="company'+count+' company">'+
                    '<input class="company-edit" name="newCompany" id="company'+count+'" type="text" oninput="checkInput('+count+')" placeholder="請輸入公司名稱">'+
                    '<div class="del-btn" id="del'+count+'" onclick="removeCompany('+count+')"></div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('beforeend', insertDiv);
}

// 新增多筆公司欄位
function saveCompanies(){
    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/Company/CreateCompany";
    
    var num = document.getElementsByName("newCompany").length-1;
    var temp = [];
    for (var i=0; i<num; i++) {
        company = document.getElementsByName("newCompany")[i].value;
        temp.push(company);
    }
    data = { "CompanyName" : temp };
    console.log(JSON.stringify(data));

    xhr.open("POST", url, true);
    var token = document.cookie.split("=")[1];
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            response = JSON.parse(xhr.response);
            
        };
    };
    xhr.send(JSON.stringify(data));
}

// 修改一筆公司欄位
function updateComapny(id) {
    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/Company/UpateCompany?Id="+id;
    var CompanyName = document.getElementById("oldCompany"+id).value;
    console.log(CompanyName);

    // 組成object並轉為Json格式
    data = { CompanyName : CompanyName };
    console.log(data);

    xhr.open("PUT", url, true);
    var token = document.cookie.split("=")[1];
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            response = JSON.parse(xhr.response);
            
        };
    };
    xhr.send(JSON.stringify(data));
}

// 刪除一筆公司欄位 (直接刪資料庫)，已存在資料庫的資料
function delCompany(id){
    var delId = '.oldCompany'+id;
    var delDiv = document.querySelector(delId);

    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/Company/DeleteCompany?Id="+id;

    xhr.open("DELETE", url, true);
    var token = document.cookie.split("=")[1];
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            response = JSON.parse(xhr.response);
            delDiv.remove();
        };
    };
    xhr.send();

}

// 刪除一筆公司欄位 (畫面上移除)，尚未進資料庫的資料
function removeCompany(id){
    var delId = '.company'+id;
    var delDiv = document.querySelector(delId);
    delDiv.remove();
}
