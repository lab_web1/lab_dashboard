
// 取得公司名稱下拉
var xhr1 = new XMLHttpRequest();
var url1 = "https://localhost:44308/api/Company/GetCompanyName";

xhr1.open("GET", url1, true);
xhr1.setRequestHeader("Content-Type", "application/json")

xhr1.onload = function () {
    if(xhr1.status >= 200 && xhr1.status < 400) {
        response1 = JSON.parse(xhr1.response);
        console.log(response1);
        GetSelect(response1);
    };
};
xhr1.send();

function GetSelect (Data) {
    for (var i in Data){
        var Id = Data[i].CompanyId;
        var Name = Data[i].CompanyName;
    
        var parentDiv = document.getElementsByClassName("company-dropdown")[0];
        var insertDiv = '<option value='+Id+'>'+Name+'</option>';
        parentDiv.insertAdjacentHTML('beforeend', insertDiv);
    }
}

// 取資料
var xhr = new XMLHttpRequest();
var url = "https://localhost:44308/api/WorkExperience/WorkDatas";

xhr.open("GET", url, true);
xhr.setRequestHeader("Content-Type", "application/json")

xhr.onload = function () {
    if(xhr.status >= 200 && xhr.status < 400) {
        response = JSON.parse(xhr.response);
        console.log(response);
        GetData(response);
    };
};
xhr.send();



function GetData (Works) {
    for (i in Works){
        console.log("new"+i);
        // ID
        var WorkId = Works[i].WorkId;
        // 名字
        var memberName = Works[i].MemberData.Name;
        // 照片

        // 身分+屆數
        var RoleId = Works[i].role.RoleId;
        var Role = Works[i].role.RoleName + " ";
        var Session = Works[i].Member.Session;
        var isGraduation = Works[i].Member.isGraduation;
        if (RoleId == 2) {
            Role += Session + "年";
        }
        else if (Role == 3) {
            Role += Session + ".0";
        }
        if (isGraduation) {
            Role += "（畢業）";
        }
        // 工作職稱
        var Position = Works[i].Position;
        // 公司名稱
        var CompanyName = Works[i].companyType.CompanyName;
        // 就職日期
        var Entry = Works[i].EntryDate.substr(0, 10).replaceAll('-', '/');
        var Leave = Works[i].LeaveDate==null ? "至今" : Works[i].LeaveDate.substr(0, 10).replaceAll('-', '/');
        var Date = Entry + "-" + Leave;


        var parentDiv = document.getElementsByClassName("works")[0];
        var insertDiv = '<div class="work" id="work'+WorkId+'" onclick="location.href=\'WorkDetail.html?Id='+WorkId+'\'">'+
                            '<div class="personal">'+
                                '<div class="person-image">'+
                                    '<img src="icon/male.jpg">'+
                                '</div>'+
                                '<div class="person-info">'+
                                    '<table>'+
                                        '<tr>'+
                                            '<td colspan="2" class="t-main">'+memberName+' / <span>'+Role+'</span></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td class="t-title"><img src="icon/position.png"></td>'+
                                            '<td class="t-content">'+Position+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td class="t-title"><img src="icon/company.png"></td>'+
                                            '<td class="t-content">'+CompanyName+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td class="t-title"><img src="icon/calendar.png"></td>'+
                                            '<td class="t-content">'+Date+'</td>'+
                                        '</tr>'+
                                    '</table>'+
                                '</div>'+
                            '</div>'+
                            '<div class="edit-btn">'+
                                '<a href="WorkEdit.html?Id='+WorkId+'"><img src="icon/edit.png"></a>'+
                            '</div>'+
                        '</div>';
        parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
    }
    newInsertbtn ();
}


// checkbox 找實習和找工作，至少勾選一項
function check(str) {
    var chkIntern = document.getElementById("chk-intern").checked;
    var chkWork = document.getElementById("chk-work").checked;

    if (chkIntern == false && chkWork == false) {
        if (str == 'intern') {
            document.getElementById("chk-work").checked = true;
        }
        else if (str = 'work') {
            document.getElementById("chk-intern").checked = true;
        }
    }
    
}


// 加入新增按鈕
function newInsertbtn (){
    var parentDiv = document.getElementsByClassName("works")[0];
    var insertDiv = '<div class="work create" onclick="location.href=\'WorkInsert.html\'">'+
                    '<img src="icon/add.png">'+
                    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
}


// 塞資料
function newWorkCard (id) {
    // 名字
    var memberName = "王小明";
    // 照片

    // 身分+屆數
    var Role = Works[i].Member.Role.RoleName + " " + Works[i].Member.Session;
    // 工作職稱
    var Position = Works[i].Position;
    // 公司名稱
    var CompanyName = Works[i].companyType.CompanyName;
    // 就職日期
    var Entry = Works[i].EntryDate.substr(0, 10).replaceAll('-', '/');
    var Leave = Works[i].LeaveDate.substr(0, 10).replaceAll('-', '/');
    var Date = Entry + "-" + Leave;


    var parentDiv = document.getElementsByClassName("works")[0];
    var insertDiv = '<div class="work" onclick="location.href=\'WorkDetail.html\'">'+
                        '<div class="personal">'+
                            '<div class="person-image">'+
                                '<img src="icon/male.jpg">'+
                            '</div>'+
                            '<div class="person-info">'+
                                '<table>'+
                                    '<tr>'+
                                        '<td colspan="2" class="t-main">'+memberName+' / <span>'+Role+'</span></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td class="t-title">工作職稱：</td>'+
                                        '<td class="t-content">'+Position+'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td class="t-title">公司名稱：</td>'+
                                        '<td class="t-content">'+CompanyName+'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td class="t-title">就職日期：</td>'+
                                        '<td class="t-content">'+Date+'</td>'+
                                    '</tr>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                        '<div class="edit-btn">'+
                            '<a href="WorkEdit.html"><img src="icon/edit.png"></a>'+
                        '</div>'+
                    '</div>';
    parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
}

