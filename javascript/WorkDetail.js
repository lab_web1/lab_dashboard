
// 取資料
window.onload = function () {
    var url = location.href;
    if(url.indexOf('?') != -1){
       str = url.split('?')[1];
    }

    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/WorkExperience/GetAWorkDatas?"+str;

    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            response = JSON.parse(xhr.response);
            console.log(response);
            GetData(response);
        };
    };
    xhr.send();
}



function GetData (Data) {
    // ID
    var WorkId = Data.work.WorkId;
    // 照片

    // 名字
    var memberName = Data.work.MemberData.Name;
    // 身分+屆數
    var RoleId = Data.work.role.RoleId;
    var Role = Data.work.role.RoleName + " ";
    var Session = Data.work.Member.Session;
    var isGraduation = Data.work.Member.isGraduation;
    if (RoleId == 2) {
        Role += Session + "年";
    }
    else if (Role == 3) {
        Role += Session + ".0";
    }
    if (isGraduation) {
        Role += "（畢業）";
    }
    document.getElementsByClassName("t-name")[0].innerHTML = memberName + " / <span>" + Role + "</span>";
    // 工作職稱
    var Position = Data.work.Position;
    document.getElementsByClassName("position-input")[0].innerText = Position;
    // 公司名稱
    var CompanyName = Data.work.companyType.CompanyName;
    document.getElementsByClassName("companyName-input")[0].innerText = CompanyName;
    // 就職日期
    var Entry = Data.work.EntryDate.substr(0, 10).replaceAll('-', '/');
    var Leave = Data.work.LeaveDate==null ? "至今" : Data.work.LeaveDate.substr(0, 10).replaceAll('-', '/');
    var Date = Entry + "-" + Leave;
    document.getElementsByClassName("work-date")[0].innerText = Date;
    // 工作內容
    var Duty = Data.work.WorkDuty;
    document.getElementsByClassName("workduty")[0].innerText = Duty;
    // 心得
    var Feedback = Data.work.Feedback;
    // document.getElementsByClassName("feedback-input")[0].innerText = Feedback;  
    if (Feedback!="") {
        var parentDiv = document.getElementsByClassName("works")[0];
        var insertDiv = '<table>\
                            <tr>\
                                <td colspan="4"><hr></td>\
                            </tr>\
                            <tr>\
                                <td class="f-title">心得：</td>\
                                <td class="f-content feedback-input">'+Feedback+'</td>\
                            </tr>\
                        </table>';
        parentDiv.insertAdjacentHTML('afterbegin', insertDiv);
    }

    // 編輯&刪除按鈕
    var parentDiv = document.getElementsByClassName("top-btn")[0];
    var insertDiv = '<div class="edit-btn"><img src="icon/edit.png" onclick="location.href=\'WorkEdit.html?'+str+'\'"></div>\
                    <div class="del-btn"><img src="icon/delete.png" onclick="DelWork()"></div>';
    parentDiv.insertAdjacentHTML('afterbegin', insertDiv);

}


// 刪除
function DelWork () {
    var xhr = new XMLHttpRequest();
    var url = "https://localhost:44308/api/WorkExperience/DeleteWorkData?"+str;

    xhr.open("DELETE", url, true);
    var token = document.cookie.split("=")[1];
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onload = function () {
        if(xhr.status >= 200 && xhr.status < 400) {
            window.location.replace("http://127.0.0.1:5500/Works.html");
        };
    };
    xhr.send();
}
