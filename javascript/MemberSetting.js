
var xhr = new XMLHttpRequest();
var url = "https://localhost:44308/api/MemberManagement/GetAMemberDatas?Id=27";

xhr.open("GET", url, true);
xhr.setRequestHeader("Content-Type", "application/json")

xhr.onload = function () {
    if(xhr.status >= 200 && xhr.status < 400) {
        response = JSON.parse(xhr.response);
        console.log(response);
        GetData(response);
    };
};
xhr.send();


// 塞資料
function GetData (Data) {

    // 照片
    // var ImageUrl = Data.ImageUrl;
    // ImageUrl = ImageUrl.replaceAll('\\','/');
    // console.log(ImageUrl);
    // document.getElementsByClassName("my-image")[0].src = "D:/Lab/LabWeb/labweb/Lab/Upload/hschiang.jpg";
    // 帳號
    var Account = response.Member.Account;
    document.getElementsByClassName("account-input")[0].value = Account;
    // 姓名
    var Name = response.Name;
    document.getElementsByClassName("name-input")[0].value = Name;
    // 性別
    var Gender = response.Gender;
    document.getElementsByClassName("gender-input")[0].value = Gender;
    // 身份
    var Role = response.Member.RoleId;
    document.getElementsByClassName("role-input")[0].value = Role;
    // 屆數
    var Session = response.Member.Session;
    document.getElementsByClassName("session-input")[0].value = Session;
    // LineId
    var LineId = response.LineId;
    document.getElementsByClassName("lineId-input")[0].value = LineId;
    // 信箱
    var Email = response.Email;
    document.getElementsByClassName("email-input")[0].value = Email;
    // 技能
    var Skill;
    
}

// 上傳圖片
function uploadImg () {
    document.getElementsByClassName("upload-real")[0].click();
}


// 顯示圖片
function showImg () {
    // var imgUrl = document.getElementsByClassName("upload-real")[0].value;
    var oFReader = new FileReader();
        var file = document.getElementsByClassName("upload-real")[0].files[0]
        oFReader.readAsDataURL(file);
        oFReader.onloadend = function(oFRevent){
            var imgUrl = oFRevent.target.result;
            document.getElementsByClassName("my-image")[0].src = imgUrl;
        }
}


// 編輯心得
function EditFeedback(str) {
    var editClass = str + "-edit";
    var btn = document.getElementsByClassName(editClass)[0];
    var feedback = document.getElementsByClassName(editClass)[0].parentElement.nextElementSibling;
    
    btn.outerHTML = '<button class="save-FB middle-btn '+str+'-save" onclick="SaveFeedback(\''+str+'\')">儲存心得</button>';
    feedback.outerHTML = '<td class="w-content" colspan="3"><textarea class="inputbox5 text-FB '+str+'-text" type="text">' + feedback.innerText + '</textarea></td>';
}


// 儲存心得
function SaveFeedback(str) {
    var saveClass = str + "-save";
    var saveText = str + "-text";
    var btn = document.getElementsByClassName(saveClass)[0];
    var feedback = document.getElementsByClassName(saveText)[0];

    btn.outerHTML = '<button class="edit-FB middle-btn '+str+'-edit" onclick="EditFeedback(\''+str+'\')">編輯心得</button>';
    feedback.outerHTML = feedback.value;
}
